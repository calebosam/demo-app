IMAGE = caleb9083/demo-app

build:
	docker build -t $(IMAGE) .

up:
	docker run -p 3000:3000 --name demo-app -d $(IMAGE) 

down:
	docker rm -f demo-app
push:
	docker push $(IMAGE)